import urllib2
import json

TOKEN = ''
CHANNEL = ''


def get_members():
	global token
    response = urllib2.urlopen("https://slack.com/api/users.list?token={}".format(TOKEN))
    data = json.load(response)
    return data.get("members", [])


def get_messages():
	global TOKEN
	global CHANNEL
    response = urllib2.urlopen("https://slack.com/api/channels.history?token={}&channel={}".format(TOKEN, CHANNEL))
    data = json.load(response)
    return data.get("messages", [])


def lambda_handler(event, context):
    messages = get_messages()
    members = get_members()

    body = ""
    for message in messages:
        if message['type'] != 'message':
            continue
        member = (m for m in members if m['id'] == message['user']).next()
        body += '<div style="margin-top:8px;"><img src="{}" style="width:32px; height:32px; vertical-align:middle;" /> {}</div>\n'.format(member['profile']['image_32'], message['text'])

    return {
        'statusCode': "200",
        'body': body,
        'headers': {
            "Content-Type": "text/html",
        },
    }
