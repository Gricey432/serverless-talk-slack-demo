# serverless-talk-slack-demo
A super simple example of a webpage in AWS Lambda used in my talk on Webapps with AWS Lambda for the [UQ Computing Society](https://uqcs.org.au)

## Usage
Insert the content of the python code into a new python 2.7 lambda function and attach an API gateway to it. You'll need to put in your [slack token](https://api.slack.com/custom-integrations/legacy-tokens) and [channel id](https://api.slack.com/methods/channels.list).

See my [full talk](https://www.youtube.com/watch?v=j5z_AtUEi9M) for detailed step by step instructions on how to deploy this code.
